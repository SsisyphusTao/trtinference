#!/usr/bin/python
import sys
sys.path.append('/home/chandler/rtspcudaproc/build/python/')
# import libnvCamera
import cv2 as cv
import numpy as np

import pycuda
import tensorrt as trt
import common

TRT_LOGGER = trt.Logger()
trt.init_libnvinfer_plugins(TRT_LOGGER, '')

# args = libnvCamera.ArgsCollection('rtsp://admin:admin123@192.168.1.176/h264/ch1/main/av_stream')
# args.cameraID = 0
# libnvCamera.target(args)

# img = libnvCamera.getFrame()
# if img.size:
#     img = cv.cvtColor(img, cv.COLOR_YUV2BGR_NV12)

def get_engine(engine_path):
    print("Reading engine from file {}".format(engine_path))
    with open(engine_path, "rb") as f, trt.Runtime(TRT_LOGGER) as runtime:
        return runtime.deserialize_cuda_engine(f.read())

# Loads a random test case from pytorch's DataLoader
def load_random_test_case(pagelocked_buffer, batchsize):
    # Select an image at random to be the test case.
    img = np.random.randn(batchsize,3,224,224).astype(np.float32)
    # Copy to the pagelocked input buffer
    np.copyto(pagelocked_buffer, img.ravel())
    return img

engine = get_engine('mobilenetv3_centernet.trt')
print(engine.max_batch_size)

with engine.create_execution_context() as context:
    inputs, outputs, bindings, stream = common.allocate_buffers(engine=engine)
    load_random_test_case(inputs[0].host, 1)
    output = common.do_inference(context, bindings=bindings, inputs=inputs, outputs=outputs, stream=stream)
    for i in output:
        print(i.shape)