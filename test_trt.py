from torch.nn.modules import batchnorm
import model
from PIL import Image
import numpy as np

import pycuda.driver as cuda
import pycuda.autoinit

import tensorrt as trt
import torch

import common
from model_torch import MobileNet2

# You can set the logger severity higher to suppress messages (or lower to display more messages).
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

class ModelData(object):
    INPUT_NAME = "data"
    INPUT_SHAPE = (3, 128, 128)
    OUTPUT_NAME = "prob"
    OUTPUT_SIZE = 4
    DTYPE = trt.float32

def _make_divisible(v, divisor, min_value=None):
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    :param v:
    :param divisor:
    :param min_value:
    :return:
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v

class MobileNetv2(object):
    def __init__(self, weights) -> None:
        super().__init__()
        self.num_of_channels = [32, 16, 24, 32, 64, 96, 160, 320]
        self.n = [1, 1, 2, 3, 4, 3, 3, 1]
        self.s = [2, 1, 2, 2, 2, 1, 2, 1]
        self.t = 6
        self.c = [_make_divisible(ch, 8) for ch in self.num_of_channels]
        self.weights = weights
        self.engine = self.build_engine()

    def add_BatchNorm2d(self, input_tensor, parent):
        gamma = self.weights[parent + '.weight'].numpy()
        beta = self.weights[parent + '.bias'].numpy()
        mean = self.weights[parent + '.running_mean'].numpy()
        var = self.weights[parent + '.running_var'].numpy()
        eps = 1e-5

        scale = gamma / np.sqrt(var + eps)
        shift = beta - mean * gamma / np.sqrt(var + eps)
        power = np.ones_like(scale)

        return self.network.add_scale(input=input_tensor.get_output(0), mode=trt.ScaleMode.CHANNEL, shift=shift, scale=scale, power=power)

    def LinearBottleneck(self, input_tensor, inplanes, outplanes, stride=1, t=6, parent=''):
        conv1_w = self.weights[parent + '.conv1.weight'].numpy()
        conv1 = self.network.add_convolution(input=input_tensor.get_output(0), num_output_maps=inplanes*t, kernel_shape=(1, 1), kernel=conv1_w)

        bn1 = self.add_BatchNorm2d(conv1, parent + '.bn1')
        relu1 = self.network.add_activation(input=bn1.get_output(0), type=trt.ActivationType.RELU)
        clip1 = self.network.add_activation(input=relu1.get_output(0), type=trt.ActivationType.CLIP)
        clip1.beta = 6

        conv2_w = self.weights[parent + '.conv2.weight'].numpy()
        conv2 = self.network.add_convolution(clip1.get_output(0), inplanes*t, (3, 3), conv2_w)
        conv2.stride = (stride, stride)
        conv2.padding = (1, 1)
        conv2.num_groups = inplanes*t

        bn2 = self.add_BatchNorm2d(conv2, parent + '.bn2')
        relu2 = self.network.add_activation(input=bn2.get_output(0), type=trt.ActivationType.RELU)
        clip2 = self.network.add_activation(input=relu2.get_output(0), type=trt.ActivationType.CLIP)
        clip2.beta = 6

        conv3_w = self.weights[parent + '.conv3.weight'].numpy()
        conv3 = self.network.add_convolution(input=clip2.get_output(0), num_output_maps=outplanes, kernel_shape=(1, 1), kernel=conv3_w)

        bn3 = self.add_BatchNorm2d(conv3, parent + '.bn3')

        if stride == 1 and inplanes == outplanes:
            out = self.network.add_elementwise(input_tensor.get_output(0), bn3.get_output(0), trt.ElementWiseOperation.SUM)
        else:
            out = bn3
        
        return out

    def _make_stage(self, input_tensor, inplanes, outplanes, n, stride, t, stage, parent):
        stage_name = parent + ".LinearBottleneck{}".format(stage)
        # First module is the only one utilizing stride
        out = stage_name + "_0"
        locals()[out] = self.LinearBottleneck(input_tensor, inplanes, outplanes, stride, t, out)
        # add more LinearBottleneck depending on number of repeats
        for i in range(n - 1):
            input_tensor = out
            out = stage_name + "_{}".format(i + 1)
            locals()[out] = self.LinearBottleneck(locals()[input_tensor], outplanes, outplanes, 1, 6, out)
        return locals()[out]

    def _make_bottlenecks(self, input_tensor, parent):
        stage_name = parent + ".Bottlenecks"

        # First module is the only one with t=1
        out = stage_name + "_0"
        locals()[out] = self._make_stage(input_tensor, inplanes=self.c[0], outplanes=self.c[1], n=self.n[1], stride=self.s[1], t=1,
                                        stage=0, parent=out)

        # add more LinearBottleneck depending on number of repeats
        for i in range(1, len(self.c) - 1):
            input_tensor = out
            out = stage_name + "_{}".format(i)
            locals()[out] = self._make_stage(locals()[input_tensor], inplanes=self.c[i], outplanes=self.c[i + 1], n=self.n[i + 1],
                                        stride=self.s[i + 1],
                                        t=self.t, stage=i, parent=out)

        return locals()[out]

    def populate_network(self):
        # Configure the network layers based on the self.weights provided.
        input_tensor = self.network.add_input(name=ModelData.INPUT_NAME, dtype=ModelData.DTYPE, shape=ModelData.INPUT_SHAPE)

        conv1_w = self.weights['conv1.weight'].numpy()
        conv1 = self.network.add_convolution(input=input_tensor, num_output_maps=self.c[0], kernel_shape=(3, 3), kernel=conv1_w)
        conv1.stride = (self.s[0], self.s[0])
        conv1.padding = (1, 1)

        bn1 = self.add_BatchNorm2d(conv1, 'bn1')
        relu1 = self.network.add_activation(input=bn1.get_output(0), type=trt.ActivationType.RELU)
        clip1 = self.network.add_activation(input=relu1.get_output(0), type=trt.ActivationType.CLIP)
        clip1.beta = 6

        bottlenecks = self._make_bottlenecks(clip1, 'bottlenecks')

        conv_last_w = self.weights['conv_last.weight'].numpy()
        conv_last = self.network.add_convolution(input=bottlenecks.get_output(0), num_output_maps=1280, kernel_shape=(1, 1), kernel=conv_last_w)

        bn_last = self.add_BatchNorm2d(conv_last, 'bn_last')
        relu_last = self.network.add_activation(input=bn_last.get_output(0), type=trt.ActivationType.RELU)
        clip_last = self.network.add_activation(input=relu_last.get_output(0), type=trt.ActivationType.CLIP)
        clip_last.beta = 6

        avgpool = self.network.add_pooling(input=clip_last.get_output(0), type=trt.PoolingType.AVERAGE, window_size=(7, 7))

        fc_w = self.weights['fc.weight'].numpy()
        fc_b = self.weights['fc.bias'].numpy()
        fc = self.network.add_fully_connected(avgpool.get_output(0), ModelData.OUTPUT_SIZE, fc_w, fc_b)

        fc.get_output(0).name = ModelData.OUTPUT_NAME
        self.network.mark_output(tensor=fc.get_output(0))

    def build_engine(self):
        # For more information on TRT basics, refer to the introductory samples.
        with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network:
            self.network = network
            builder.max_workspace_size = common.GiB(2)
            builder.max_batch_size = 32
            # Populate the network using self.weights from the PyTorch model.
            self.populate_network()
            # Build and return an engine.
            return builder.build_cuda_engine(self.network)
batchsize = 4
# Loads a random test case from pytorch's DataLoader
def load_random_test_case(pagelocked_buffer):
    # Select an image at random to be the test case.
    img = np.random.randn(32, 3,128,128).astype(np.float32)
    # Copy to the pagelocked input buffer
    np.copyto(pagelocked_buffer, img.ravel())
    return img[:batchsize]

def log_softmax(x):
    return np.log(np.exp(x) / np.sum(np.exp(x), axis=1, keepdims = True))

def main():
    common.add_help(description="Runs an MNIST network using a PyTorch model")
    # Get the PyTorch weights
    weights = torch.load('best_97.62_addqinghua.pth', map_location={'cuda:1':'cpu'})
    mobilenetv2 = MobileNet2(input_size=128, num_classes=4)
    mobilenetv2.load_state_dict(weights)
    mobilenetv2.eval()
    # Do inference with TensorRT.
    with MobileNetv2(weights).engine as engine:
        # with open('qinghua_classifier.trt', "wb") as f:
        #     f.write(engine.serialize())
        # Build an engine, allocate buffers and create a stream.
        # For more information on buffer allocation, refer to the introductory samples.
        inputs, outputs, bindings, stream = common.allocate_buffers(engine)
        with engine.create_execution_context() as context:
            img = load_random_test_case(pagelocked_buffer=inputs[0].host)
            # For more information on performing inference, refer to the introductory samples.
            # The common.do_inference function will return a list of outputs - we only have one in this case.
            [output] = common.do_inference(context, bindings=bindings, inputs=inputs, outputs=outputs, stream=stream, batch_size=batchsize)
            with torch.no_grad():
                baseline = mobilenetv2(torch.from_numpy(img)).numpy()
                print('baseline: ', baseline)
                case_num = np.argmax(baseline)
            print('output:   ', output[:batchsize*4].reshape(baseline.shape))
            pred = np.argmax(output)
            print("Test Case: " + str(case_num))
            print("Prediction: " + str(pred))

if __name__ == '__main__':
    main()