#!/usr/bin/python
import tensorrt as trt
import pycuda
import pycuda.driver as cuda
import cv2
import numpy as np
import time
from utils_trt import *

TRT_LOGGER = trt.Logger()

class HostDeviceMem(object):
    def __init__(self, host_mem, device_mem):
        self.host = host_mem
        self.device = device_mem

    def __str__(self):
        return "Host:\n" + str(self.host) + "\nDevice:\n" + str(self.device)

    def __repr__(self):
        return self.__str__()

class YoloTRT(object):
    def __init__(self, engine_path):
        self.num_classes = 1
        self.IN_IMAGE_H = 608
        self.IN_IMAGE_W = 608
        self.engine = self.get_engine(engine_path)
        self.infer_context = self.engine.create_execution_context()
        self.buffers = self.allocate_buffers(1)
        self.infer_context.set_binding_shape(0, (1, 3, self.IN_IMAGE_H, self.IN_IMAGE_W))
    
    def get_engine(self, engine_path):
        print("Reading engine from file {}".format(engine_path))
        with open(engine_path, "rb") as f, trt.Runtime(TRT_LOGGER) as runtime:
            return runtime.deserialize_cuda_engine(f.read())
    
    def allocate_buffers(self, batch_size):
        inputs = []
        outputs = []
        bindings = []
        stream = cuda.Stream()
        
        for binding in self.engine:
            size = trt.volume(self.engine.get_binding_shape(binding))*batch_size
            dims = self.engine.get_binding_shape(binding)
            
            if dims[0] < 0:
                size *= -1
            
            dtype = trt.nptype(self.engine.get_binding_dtype(binding))
            host_mem = cuda.pagelocked_empty(size, dtype)
            device_mem = cuda.mem_alloc(host_mem.nbytes)
            bindings.append(int(device_mem))
            
            if self.engine.binding_is_input(binding):
                inputs.append(HostDeviceMem(host_mem, device_mem))
            else:
                outputs.append(HostDeviceMem(host_mem, device_mem))
        return inputs, outputs, bindings, stream
    
    def detect(self, img):
        img_width = img.shape[1]
        img_height = img.shape[0]
        resized = cv2.resize(img, (self.IN_IMAGE_W, self.IN_IMAGE_H))
        img_in = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
        img_in = np.transpose(img_in, (2, 0, 1)).astype(np.float32)
        img_in = np.expand_dims(img_in, axis=0)
        img_in /= 255.0
        img_in = np.ascontiguousarray(img_in)
        inputs, outputs, bindings, stream = self.buffers
        inputs[0].host = img_in
        trt_outputs = self.do_inference(bindings=bindings, inputs=inputs, outputs=outputs, stream=stream)
        if trt_outputs == False:
            raise RuntimeError('TrtInferError')
        #print(trt_outputs)
        trt_outputs[0] = trt_outputs[0].reshape(1, -1, 1, 4)
        trt_outputs[1] = trt_outputs[1].reshape(1, -1, self.num_classes)
        boxes = post_processing(img_in, 0.7, 0.5, trt_outputs)
        for i in range(len(boxes[0])):
            boxes[0][i][0] = boxes[0][i][0]*img_width
            boxes[0][i][2] = boxes[0][i][2]*img_width
            boxes[0][i][1] = boxes[0][i][1]*img_height
            boxes[0][i][3] = boxes[0][i][3]*img_height
        return boxes
    
    def do_inference(self, bindings, inputs, outputs, stream):
        [cuda.memcpy_htod_async(inp.device, inp.host, stream) for inp in inputs]
        tmp_flag = self.infer_context.execute_async(bindings=bindings, stream_handle=stream.handle)
        [cuda.memcpy_dtoh_async(out.host, out.device, stream) for out in outputs]
        stream.synchronize()
        #time.sleep(10)
        return [out.host for out in outputs]

if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    import pycuda.autoinit
    import sys
    sys.path.append('/home/chandler/rtspcudaproc/build/python/')
    import libnvCamera

    args = libnvCamera.ArgsCollection('rtsp://admin:admin123@192.168.1.176/h264/ch1/main/av_stream')
    args.cameraID = 0
    libnvCamera.target(args)
    
    model = YoloTRT('yolov4_1118.trt')
    
    while not cv2.waitKey(1) == ord('q'):
        img = libnvCamera.getFrame()
        img = cv2.cvtColor(img, cv2.COLOR_YUV2BGR_NV12)
        boxes = model.detect(img)
        for box in boxes[0]:
            if box[-1] == 0:
                cv2.rectangle(img, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (0,255,0), 2)
            elif box[-1] == 1:
                cv2.rectangle(img, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (0,0,255), 2)
            else:
                cv2.rectangle(img, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), 255, 2)

        cv2.imshow('office', img)
    
