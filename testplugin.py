import cv2 as cv
import numpy as np

import tensorrt as trt
import common

import torch
import time

TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
trt.init_libnvinfer_plugins(TRT_LOGGER, '')

camera = cv.VideoCapture('rtsp://admin:admin123@192.168.1.57/h264/ch1/main/av_stream')
#camera = CSICamera(width=1280, height=720, capture_width=1280, capture_height=720, capture_fps=30)
# while not cv.waitKey(1)== ord('q'):
#   cv.imshow('c', camera.read())


with open('yolov5.trt', "rb") as f, trt.Runtime(TRT_LOGGER) as runtime, runtime.deserialize_cuda_engine(f.read()) as engine:
  inputs, outputs, bindings, stream = common.allocate_buffers(engine)
  with engine.create_execution_context() as context:

    while not cv.waitKey(1)== ord('q'):
      f, img = camera.read()
      if not f:
        continue
      dis = img.copy()
      a = time.time()
      img = cv.resize(img, (512,288))
      img = cv.cvtColor(img.astype(np.float32), cv.COLOR_RGB2BGR)
      img /= 255.0
      img = np.transpose(img,(2,0,1))
      # Copy to the pagelocked input buffer
      np.copyto(inputs[0].host, img.ravel())
      [kc, nb, conf, cls] = common.do_inference(context, bindings=bindings, inputs=inputs, outputs=outputs, stream=stream, batch_size=1)

      for i in range(kc[0]):
          cv.rectangle(dis, (int(nb[i*4]*1920), int(nb[i*4+1]*1080)), (int(nb[i*4+2]*1920), int(nb[i*4+3]*1080)), 255, 1)
      cv.imwrite('output.jpg', dis)

      print(time.time()-a)